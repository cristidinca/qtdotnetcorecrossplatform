﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QtDotNetCoreCP.model
{
    public class DriveModel
    {
        public string Drive { get; set; }
        public List<string> Directories { get; set; }
        public List<string> Files { get; set; }
    }
}
