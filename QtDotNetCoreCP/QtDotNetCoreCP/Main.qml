import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1
import Main 0.1

ApplicationWindow {
	id: window
	visible: true
	width: 640
	height: 800
	title: qsTr("Main")

	header: ToolBar {
		Material.foreground: "white"

		RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                icon.source: stackView.depth > 1 ? "images/back.png" : "images/drawer.png"
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                        listView.currentIndex = -1
                    } else {
                        drawer.open()
                    }
                }
            }

            Label {
                id: titleLabel
                text: listView.currentItem ? listView.currentItem.text : "Gallery"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                icon.source: "images/menu.png"
                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: "About"
                        onTriggered: aboutDialog.open()
                    }
                }
            }
        }
	}

	StackView {
		id: stackView
		anchors.fill: parent

		initialItem: Pane {
			id: pane
			Image {
				id: logo
				width: pane.availableWidth / 2
				height: pane.availableHeight / 2
				anchors.centerIn: parent
				anchors.verticalCenterOffset: -50
				fillMode: Image.PreserveAspectFit
				source: "images/images.png"
			}			

			Label {
				text: "Qml.Net is a bridge between .NET and Qml, allowing you to build powerfull user-interfaces driven by the .NET Framework."
				anchors.margins: 20
				anchors.top: logo.bottom
				anchors.left: parent.left
				anchors.right: parent.right
				//anchors.bottom: arrow.top
				horizontalAlignment: Label.AlignHCenter
				verticalAlignment: Label.AlignVCenter
				wrapMode: Label.Wrap
			}
		}
		Component.onCompleted: {
			//console.log("------------>" + pane.logo.source);
		}

	}

	Drawer {
		id: drawer
		width: Math.min(window.width, window.height) / 3 * 2
		height: window.height
		interactive: stackView.depth === 1

		ListView {
            id: listView
            focus: true
            currentIndex: -1
            anchors.fill: parent

            delegate: ItemDelegate {
                width: parent.width
                text: model.title
				Rectangle {
					id: rectangle
					width: parent.width
					height: parent.height
					radius: 8 // This gives rounded corners to the Rectangle
					gradient: Gradient { // This sets a vertical gradient fill
						GradientStop { position: 0.0; color: area.containsMouse  ? "aqua" : "teal"}
						GradientStop { position: 1.0; color: area.containsMouse  ? "teal" : "aqua" }
					}
					border { width: 3; color: area.containsMouse  ? "yellow" : "blue"} // This sets a 3px wide black border to be drawn
					MouseArea {
						id: area
						//anchors.fill: parent
						width: parent.width
						height: parent.height
						hoverEnabled: true
						onClicked: {
							listView.currentIndex = index
							stackView.push(model.source)
							drawer.close()
						}
						/*onEntered: { -> not really needed for ui changes, we have containsMouse :)
							rectangle.border.color = "green"	
						}
						onExited: {
							rectangle.border.color = "red"	
						}*/
					}
				}
                //highlighted: ListView.isCurrentItem
                /*onClicked: { --> swallowd by MouseArea
                    listView.currentIndex = index
                    stackView.push(model.source)
                    drawer.close()
                }*/
            }

            model: ListModel {
                /*ListElement { title: "Signals"; source: "pages/Signals.qml" }
                ListElement { title: "Notify signals"; source: "pages/NotifySignals.qml" }
                ListElement { title: "Async/await"; source: "pages/AsyncAwait.qml" }
                ListElement { title: ".NET objects"; source: "pages/NetObjects.qml" }
                ListElement { title: "Dynamics"; source: "pages/Dynamics.qml" }
                ListElement { title: "Simple calculator"; source: "pages/Calculator.qml" }*/
				ListElement { title: "File System"; source: "views/FileView.qml" }
                ListElement { title: "Collections"; source: "views/Collections.qml" }
            }

            ScrollIndicator.vertical: ScrollIndicator { }
		}
	}
}