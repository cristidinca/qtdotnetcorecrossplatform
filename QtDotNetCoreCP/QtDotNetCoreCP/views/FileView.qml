import QtQuick 2.9
import QtQuick.Controls 2.3
import Main 0.1

ListView {
	Column {
		spacing: 40
		width: parent.width

		Label {
			width: parent.width
			wrapMode: Label.Wrap
			horizontalAlignment: Qt.AlignCenter
			text: "File System"
		}

		Repeater {
			id: repeater
			Column {
				Text {
					text: model.stringify(drive);
				}
			}
		}

		FileViewModel {
			id: fileViewModel
			
			Component.onCompleted: {
				repeater.model = Net.toListModel(fileViewModel.drives)

			}
		}
	}
}