import QtQuick 2.9
import QtQuick.Controls 2.3
import Main 0.1

ListView {
	Column {
		spacing: 40
		width: parent.width

		Label {
			width: parent.width
			wrapMode: Label.Wrap
			horizontalAlignment: Qt.AlignCenter
			text: "A List<Test> is bound to a Repeater"
		}

		Row {
			TextField {
				id: testProperty
				placeholderText: "testProperty"
			}
		}

		Button {
			text: "Add Test object"
			onClicked: {
				if(collectionsModel.addTest(testProperty.Text)) {
					repeater.model = Net.toListModel(collectionsModel.tests)
					testProperty.text = null;
				}
			}
		}

		Repeater {
			id: repeater
			Column {
				Text {
					text: "Test Property: " + modelData.testProperty
				}
				Button {
					text: "Remove"
					onClicked: {
						collectionsModel.removeTest(index)
						repeater.model = Net.toListModel(collectionsModel.tests)
					}
				}
			}
		}

		Text {
			id: message
		}

		CollectionsModel {
			id: collectionsModel
			Component.onCompleted: {
				repeater.model = Net.toListModel(collectionsModel.tests)
			}
		}
	}
}