﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QtDotNetCoreCP.viewModel
{
    public class FileViewModel
    {
        private List<model.DriveModel> _drives;
        public List<model.DriveModel> Drives { get => _drives; }


        private List<DirectoryInfo> _directories;
        public List<DirectoryInfo> Directories { get => _directories; }

        public FileViewModel()
        {
            _drives = new List<model.DriveModel>();
            //_directories = new List<DirectoryInfo>();
            foreach (string drive in Environment.GetLogicalDrives())
            {
                _drives.Add(new model.DriveModel() { Drive = drive, Directories = new List<string>(), Files = new List<string>() });
            }
        }

        public void GetDirectories()
        {
            //_directories = Directory.EnumerateDirectories
        }
    }
}
