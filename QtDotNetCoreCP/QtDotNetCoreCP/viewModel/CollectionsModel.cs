﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QtDotNetCoreCP.viewModel
{
    public class CollectionsModel
    {
        private readonly List<Test> _tests = new List<Test>()
        {
            new Test()
            {
                TestProperty = "test0"
            }
        };

        public List<Test> Tests { get => _tests; }

        public bool AddTest(string testProperty)
        {
            if (string.IsNullOrEmpty(testProperty) || string.IsNullOrWhiteSpace(testProperty))
                return false;

            _tests.Add(new Test() { TestProperty = testProperty });
            return true;
        }

        public void RemoveTest(int index)
        {
            //TODO check index
            _tests.RemoveAt(index);
        }

        public class Test
        {
            public string TestProperty { get; set; }
        }
    }
}
