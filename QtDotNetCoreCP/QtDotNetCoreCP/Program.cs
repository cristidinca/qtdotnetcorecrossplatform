﻿using System;
using System.Reflection;

namespace QtDotNetCoreCP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowPosition(0, 0);
            //Console.SetWindowSize(1,1);
            //Console.WriteLine("Hello World!");
            try
            {
                QtApp(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.InnerException);
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            Console.ReadKey(true);
        }

        static int QtApp(string[] args)
        {
            using (var app = new Qml.Net.QGuiApplication(args))
            {
                using (var engine = new Qml.Net.QQmlApplicationEngine())
                {
                    Qml.Net.Qml.RegisterType<MainModel>("Main", 0, 1);
                    Qml.Net.Qml.RegisterType<viewModel.CollectionsModel>("Main", 0, 1);

                    Qml.Net.Qml.RegisterType<viewModel.FileViewModel>("Main", 0, 1);
                    Qml.Net.Qml.RegisterType<model.DriveModel>("Main", 0, 1);

                    engine.SetContextProperty("applicationDirPath", AppContext.BaseDirectory.Replace(@"\",@"/"));
                    engine.Load("Main.qml");
                    return app.Exec();
                }
            }
        }
    }
}
